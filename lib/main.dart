import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: firstRoute(),
  ));
}

class firstRoute extends StatelessWidget {
  const firstRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('First Route')),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
        ElevatedButton(
          child: Text('Open Route 2 '),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return secondRoute();
            }));
          },
        ),
        SizedBox(
          height: 10.0,
        ),
        ElevatedButton(
          child: Text('Open Route 3 '),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return thirdRoute();
            }));
          },
        ),
      ])),
    );
  }
}

class secondRoute extends StatelessWidget {
  const secondRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Second Route')),
      body: Center(
        child: ElevatedButton(
          child: Text('Go Back'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

class thirdRoute extends StatelessWidget {
  const thirdRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Third Route')),
      body: Center(
        child: ElevatedButton(
          child: Text('Go Back'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
